package bo.mc4.edson.pruebas.web;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import org.apache.log4j.Logger;
import org.junit.Test;

public class LoginServiceTest {

	private String usuario = "";
	private String contrasenia = "";
	private final static Logger log = Logger.getLogger(LoginServiceTest.class);

	@Test
	public void loginExitoso() {
		usuario = "admin";
		contrasenia = "admin";
		assertSame("ok",usuario, "admin");
		assertSame("ok",contrasenia, "admin");
		assertTrue(true);	
	}
	
	@Test
	public void loginFallido() {
		usuario = "admin";
		contrasenia = "123";
		assertSame("ok",usuario, "admin");
		assertSame("ok",contrasenia, "123");
		assertTrue(true);	
	}
	
	@Test
	public void loginSinParametros() {
		usuario = null;
		contrasenia = null;
		assertNull(usuario);
		assertNull(contrasenia);
		assertTrue(true);	
	}

}